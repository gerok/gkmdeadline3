'use strict';
/**
 *Mario ist eine Funktion in der ein Mario gezeigt wird, der die Möglichkeit hat zu springen und mit einer Box zu kollidieren.
 * @constructor
 */
function Mario() {
	/**
	 *Eine Variabel mit dem Namen: jumpHeight, die die Sprunghöhe von 200 pixeln angibt.
	 * @type {number}
	 */
	var jumpHeight = 200;
	/**
	 * Eine Variabel mit dem Namen: size, die eine Größe definiert von 50 Pixeln.
	 * @type {number}
	 */
	var size = 50;
	/**
	 * Hier wird die Funktion Box aufgerufen in der eine Neue Box erstellt wird, welche in einer anderen Klasse programmiert wurde.
	 * @type {Box}
	 * @param {number} x - 50 Pixel zur Seite
	 * @param {number} y - Variabel: Bodentiefe (spawnt also auf Bodenhöhe des Bereiches)
	 * @param {number} width - Variabel: size (also 50 Pixel)
	 * @param {number} height - Variabel: size
	 */
	this.box = new Box(50, GROUNDLEVEL, size, size);
	/**
	 * Eine Variabelmit dem Namen: velocityY, die eine Geschwindigkeit, die vermutlich in Y-Achsen-Richtung verläuft, darstellt.
	 * Diese Variabel wurde erstmal auf 0 definiert.
	 * @type {number}
	 */
	var velocityY = 0;
	/**
	 *Hier wird die Funktion des Mariosprunges dargestellt. Sollte diese verwendet werden, soll die Konsole "jump" ausgeben.
	 * Die Variabel "velocityY" wird in der Funktion verändert, sodass das Bild von Mario springt.
	 * Diese ist negativ weil die Y-Achse sich von oben nach unten ausbreitet.
	 * @member {object}
	 */
	this.jump = function () {
		console.log("jump");
		velocityY = -2;
	}
	/**
	 *Hier wird die Funktion der Darstellung gezeigt.
	 * @member {object}
	 */
	this.draw = function () {
		/**
		 *Die Y-Position der Box wird mit die Variabel: velocityY verrechnet und neu definiert.
		 */
		this.box.y = this.box.y + velocityY;
		/**
		 * Wenn die Y-Position kleiner als die Sprunghöhe des Marios ist wird der Wert von der Variabel "veloctyY" auf 4 Pixel gesetzt,
		 * was die Fall-geschwindigkeit von Mario zeigt.
		 */
		if (this.box.y < jumpHeight)
			velocityY = 4;
		/**
		 *Ist die Y-Position größer-gleich der bodentiefe wird die Variabel "velocityY" auf null Pixel gesetzt,
		 * wodurch Mario nicht weiter fällt.
		 */
		if (this.box.y >= GROUNDLEVEL)
			velocityY = 0;
		/**
		 *Bildausgabe des Marios mit x unx y Position und die Variabel "size" für Breite und Höhe.
		 * @function
		 * @param {object} Mario
		 * @param {number} x - this.box.left
		 * @param {number} y - this.box.left
		 * @param {number} width - size
		 * @param {number} height - size
		 */
		image(images.mario, this.box.left(), this.box.top(), size, size);
	}
}
