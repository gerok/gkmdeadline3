/**
 * Created by gerok on 01.11.2016.
 */

function Ballon () {
    this.y = 25;
    this.x = width/2;

    this.gravitation = 0.4;
    this.geschwindigkeit =0;
    this.lift=-8;

    this.show = function () {
        fill(22,180,22); // Grün
        ellipse(this.y, this.x, 16, 16); // Ballon
    }
    this.up = function(){
        this.geschwindigkeit += this.lift;
    }
    this.update = function () {
        this.geschwindigkeit += this.gravitation;
        this.geschwindigkeit *= 0.9;
        this.x += this.geschwindigkeit;

        if (this.x > width) {
            this.x = width;
            this.geschwindigkeit = 0;
        }
        if (this.x < 0){
            this.x = 0;
            this.geschwindigkeit=0;
        }

    }
 }